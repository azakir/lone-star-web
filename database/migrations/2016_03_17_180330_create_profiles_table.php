<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('bio')->nullable();
            $table->text('signature')->nullable();
            $table->string('eveapi_keyID')->nullable();
            $table->string('eveapi_vCode')->nullable();
            $table->string('eve_character_image')->nullable();
            $table->string('characterID')->nullable();
            $table->string('steam_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
