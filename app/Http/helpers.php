<?php

function flash($message)
{
    session()->flash('message', $message);
}

function link_to_profile($text = 'Profile')
{
    return link_to_route('profile', $text, Auth::user()->username);
}