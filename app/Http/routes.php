<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::get('/', ['as' => 'home', 'uses' => 'PagesController@index']);

    # Registration
    Route::get('register', 'RegistrationController@register');
    Route::post('register', 'RegistrationController@postRegister');

    Route::get('register/confirm/{token}', 'RegistrationController@confirmEmail');

    # Authentication
    Route::get('login', 'SessionsController@login');
    Route::post('login', 'SessionsController@postLogin');
    Route::get('logout', 'SessionsController@logout');

# Profiles
    Route::resource('profile', 'ProfilesController', ['only' => ['show', 'edit', 'update']]);
    Route::get('/{profile}', ['as' => 'profile', 'uses' => 'ProfilesController@show']);
//Route::get('/{profile}/edit', ['as' => 'profile.edit', 'uses' => 'ProfilesController@edit']);

    Route::get('dashboard', ['middleware' => 'auth', function() {
        return 'Auth complete!';
    }]);
});


