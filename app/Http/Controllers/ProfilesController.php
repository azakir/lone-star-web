<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\User;
use Illuminate\Support\Facades\Input;
use View;
use Response;
use App\Http\Requests;

class ProfilesController extends Controller
{

    /**
     * @param int $username
     * @return Response
     */
    public function show($username)
    {
        try
        {
            $user = User::with('profile')->whereUsername($username)->firstOrFail();
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->home();
        }
        return View::make('profiles.show')->withUser($user);
    }

    public function edit($username)
    {
        $user = User::whereUsername($username)->firstOrFail();

        return View::make('profiles.edit')->withUser($user);
    }

    public function update($username)
    {
        $user = User::whereUsername($username)->firstOrFail();
        $input = Input::only('bio', 'eveapi_keyID', 'eveapi_vCode', 'steam_name');

        $this->validate($input, [
            'bio' => 'required|max:255',
            'eveapi_keyID' => 'required',
            'eveapi_vCode' => 'required',
            'steam_name' => 'required'
        ]);

        $user->profile->fill($input)->save();

        return \Redirect::route('profile.edit', $user->username);
    }
}
