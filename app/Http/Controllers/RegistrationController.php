<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Mailers\AppMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RegistrationController extends Controller
{
     /**
     * Show the register page.
     *
     * @return \Response
     */
    public function register()
    {
        if (Auth::check()) return Redirect::home();
        return view('auth.register');
    }

     /**
     * Perform the registration.
     *
     * @param  Request   $request
     * @param  AppMailer $mailer
     * @return \Redirect
     */
    public function postRegister(Request $request, AppMailer $mailer)
    {
        // Validate request
        $this->validate($request, [
            'username'      => 'required|max:255|unique:users',
            'email'         => 'required|email|max:255|unique:users',
            'password'      => 'required|min:6|confirmed',
        ]);
        // Create the user
        $user = User::create($request->all());

        // Send the confirmation Email
        $mailer->sendEmailConfirmationTo($user);

        // Flash confirmation message
        flash('Please confirm your email address.');

        // Redirect
        return redirect()->back();
    }

    public function confirmEmail($token)
    {
        User::whereToken($token)->firstOrFail()->confirmEmail();

        flash('You are now confirmed. Please login.');

        return redirect('login');
    }
}
