<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pheal\Pheal;
use Pheal\Core\Config;
use App\Http\Requests;

class CharacterController extends Controller
{
    //
}

$keyID = 4748509;
$vCode = "iQwpR3Bd4KBvV8cccoh3zFfGjkAf5Owv7JWOKg7t0GHNPktQOChnCiJUshDSywQN";

Config::getInstance()->cache = new \Pheal\Cache\FileStorage('../phealcache/');

Config::getInstance()->access = new \Pheal\Access\StaticCheck();

$pheal = new Pheal($keyID, $vCode, "account");

try {
    // parameters for the request, like a characterID can be added
    // by handing the method an array of those parameters as argument
    $response = $pheal->apikeyinfo();
    foreach ($response->key->characters as $character) {
        echo 'Character Name: '.$character->characterName.' with the ID: '.$character->characterID.'<br />'
        ;
    };
// there is a variety of things that can go wrong, like the EVE API not responding,
// the key being invalid, the key not having the rights to call the method
// or the characterID beeing wrong - just to name a few. So it is basically
// a good idea to catch Exceptions. Usually you would want to log that the
// exception happend and then decide how to inform the user about it.
// In this example we simply catch all PhealExceptions and display their message
} catch (\Pheal\Exceptions\PhealException $e) {
    echo sprintf(
        "an exception was caught! Type: %s Message: %s",
        get_class($e),
        $e->getMessage()
    );
}
?>