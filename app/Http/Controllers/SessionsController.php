<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;

class SessionsController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'password' => 'required']);

        if (Auth::attempt($this->getCredentials($request))) {
            flash('You are now logged in, sending you back to the previous page.');

        return redirect()->intended('/');
        }

        flash('Could not sign you in. The credentials you entered are incorrect or you forgot to confirm you email adress.');

        return redirect()->back();
    }

    public function logout()
    {
        Auth::logout();

        flash('You have successfully logged out. See you soon.');

        return redirect('/');
    }

    protected function getCredentials(Request $request)
    {
        return [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'verified' => true];
    }
}
