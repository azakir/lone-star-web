<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'bio', 'eveapi_keyID',
        'eveapi_vCode', 'steam_name'
    ];

    public function user()
    {
        return $this->belongsTo('User');
    }
}
