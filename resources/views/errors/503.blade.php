<!DOCTYPE html>
<html>
    <head>
        <title>Coming Soon.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        {{ Html::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') }}
        {{ Html::style('css/custom.css') }}
        <style>
            html, body {
                height: 100%;
                color: #B0BEC5;
            }

            body {
                background-color: rgba(53, 58, 65, 0.8) !important;
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                {{ Html::image('img/Logo.png') }}
                <div class="title">We will back soon!</div>
            </div>


            <p><h3><strong>Lone Star Warriors Web Platform</strong></h3></p>
            <p><h4><strong>Follow us on social media to stay up2date on recent development changes!</strong></h4></p>
                <ul class="social">
                    <li>
                        <a href=
                           "https://www.facebook.com/lonestarwarriorsgaming"><i class="fa">
                            </i></a>
                    </li>

                    <li>
                        <a href="https://twitter.com/lone_warriors"><i class=
                                                                       "fb"></i></a>
                    </li>

                    <li>
                        <a href=
                           "https://plus.google.com/+lonestarwarriorsgaming"><i class="fc">
                            </i></a>
                    </li>
                </ul>

                <p><h2>
                    &copy; 2015-<?php echo date("Y") ?> by <a href="#">Joery Pigmans</a>. All rights
                    reserved.
                </h2></p>
            </div>
        </div>
    </body>
</html>
