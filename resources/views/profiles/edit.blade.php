@extends('layouts.app')

@section('title')
    Edit Profile
@stop

@section('content')
    {!! csrf_field() !!}
    <div class="container">
    <h1>Edit Profile</h1>
        {{ Form::model($user->profile, ['route' => ['profile.update', $user->username]]) }}
        <!-- Bio Field -->
        <div class="form-group">
            {{ Form::label('bio', 'Bio:') }}
            {{ Form::textarea('bio', null, ['class' => 'form-control']) }}
        </div>

        <!-- Eve Online KeyID -->
        <div class="form-group">
            {{ Form::label('eveapi_keyID', 'EvE Online KeyID:') }}
            {{ Form::text('eveapi_keyID', null, ['class' => 'form-control']) }}
        </div>

        <!-- Eve Online vCode -->
        <div class="form-group">
            {{ Form::label('eveapi_vCode', 'EvE Online verification Code:') }}
            {{ Form::text('eveapi_vCode', null, ['class' => 'form-control']) }}
        </div>

        <!-- Steam Name -->
        <div class="form-group">
            {{ Form::label('steam_name', 'Steam Name:') }}
            {{ Form::text('steam_name', null, ['class' => 'form-control']) }}
        </div>

        <div class="form-group">
            {{ Form::submit('Update Profile', ['class' => 'btn btn-primary']) }}
        </div>
    </div>
    {{ Form::close() }}
@stop