@extends('layouts.app')

@section('title')
    Profile
@stop

@section('content')
    <div class="container">
        <h1>{{ $user->username }} <small>Steam Name: {{ $user->profile->steam_name }}</small></h1>
        <div class="bio">
            <p>
                 {{ $user->profile->bio }}
            </p>
        </div>
        <ul class="links">
            <li>{{ $user->profile->eveapi_keyID }}</li>
            <li>{{ $user->profile->eveapi_vCode }}</li>
        </ul>

        @if(Auth::user()->id == $user->id)
            {{ link_to_route('profile.edit', 'Edit your Profile', $user->username) }}
        @endif
    </div>
@stop