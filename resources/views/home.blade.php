@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <h1>{{ Auth::check() ? "Welcome, " . Auth::user()->username : "Welcome, Guest." }}</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut, cupiditate deserunt dicta earum eius exercitationem fugit libero minima nisi numquam odit rem sed sequi suscipit temporibus totam vel voluptatibus voluptatum.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
