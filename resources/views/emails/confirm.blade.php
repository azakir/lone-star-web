<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Lone Star Web email confirmation</title>
</head>
<body>
<h1>Thank you {{ $user->username }} for signing up for the Lone Star Web App!</h1>
<p>
    All that is left to do now is to <a href='{{ url("register/confirm/{$user->token}") }}'>confirm your email address</a>.
</p>
</body>
</html>