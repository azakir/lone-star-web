<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;
    /** @test */
    public function a_user_may_register_for_an_account_but_must_confirm_their_email_address()
    {
        // When we register...
        $this->visit('register')
            ->type('JohnDoe', 'username')
            ->type('john@example.com', 'email')
            ->type('password', 'password')
            ->press('Register');
        // We should have an account - but one that is not yet confirmed/verified.
        $this->see('Please confirm your email address')
            ->seeInDatabase('users', ['username' => 'JohnDoe', 'verified' => 0]);

    }
}
